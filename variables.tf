# Variables.tf file defining the variables that need to be initialized.
# Check the terraform.tfvars file for the values for those variables.

# Azure Resource Group 
variable "resource_group_name" {
  description = "The name of your Azure Resource Group."
  default     = "tf-azure-pcoca-demo"
}
variable "resource_group_location" {
  description = "The region where the virtual network is created."
  default     = "uksouth"
}

# Networking Resources 
variable "virtual_network_name" {
  description = "The name for your virtual network."
  default     = "vnet"
}

variable "virtual_network_address_space" {
    description = "The address space that is used by the virtual network."
    default     = ["10.0.0.0/16"]
}

variable "subnet_name" {
  description = "The subnet name"
  default     = "tf-azure-pcoca-demo-subnet"
}

variable "subnet_address_prefix" {
  description = "The address prefix to use for the subnet."
  default     = ["10.0.10.0/24"]
}

variable "network_interface_name" {
  description = "The Network Interface name"
  default     = "tf-azure-pcoca-demo-nic"
}

variable "network_interface_ip_configuration_name" {
  description = "The Network Interface IP Configuration name"
  default     = "tf-azure-pcoca-demo-nic-ip-config"
}

variable "public_ip_name" {
  description = "The Public IP name"
  default     = "public_ip"
}

variable "public_ip_domain_name_label" {
  description = "The Public IP domain label name"
  default     = "tfdemo"
}

# Virtual Machine Resources
variable "virtual_machine_name" {
  description = "Name of the Virtual Machine"
  default     = "VM001"
}

variable "virtual_machine_vm_size" {
  description = "Specifies the size of the virtual machine."
  default     = "Standard_B4ms"
}

variable "private_key_filename" {
  description = "Private key file name in order to ssh to the VM"
  default     = "demo-private-key.pem"
}